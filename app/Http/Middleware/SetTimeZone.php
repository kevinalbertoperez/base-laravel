<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\Configuration;
class SetTimeZone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $configuration=Configuration::first();
        // date_default_timezone_set($configuration->time_zone?$configuration->time_zone:'Europe/Madrid');
        return $next($request);
    }
}

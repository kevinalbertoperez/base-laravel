<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $fillable=[
    	'id',
        'name',
    	'slug',
		'description',
		'created_at',
		'updated_at',
	];

    public function menu()
    {
        return $this->belongsToMany('App\Http\Models\Menu', 'role_menus', 'role_id', 'menu_id');
    }
    public function users()
    {
        return $this->hasMany('App\User', 'role_id', 'id');
    }
}

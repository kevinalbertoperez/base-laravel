<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    //
    protected $fillable = [
		'key',
		'key_value',
		'key_description',
	];
}

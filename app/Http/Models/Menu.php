<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $fillable=[
    	'id',
    	'name',
		'route',
		'ico',
		'position',
		'created_at',
		'updated_at',
	];
}

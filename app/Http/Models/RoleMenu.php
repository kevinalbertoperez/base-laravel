<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    //
    protected $fillable=[
    	'id',
    	'role_id',
		'menu_id',
		'created_at',
		'updated_at',
	];

    public function menu()
    {
        return $this->hasOne('App\Http\Models\Menu', 'id', 'menu_id');
    }

    public function role()
    {
        return $this->hasOne('App\Http\Models\Role', 'id', 'role_id');
    }
}

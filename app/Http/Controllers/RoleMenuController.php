<?php

namespace App\Http\Controllers;

use App\Http\Models\RoleMenu;
use App\Http\Models\Menu;
use App\Http\Models\Role;
use Illuminate\Http\Request;

class RoleMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rolemenus = RoleMenu::latest()->paginate(5);
        return view('role-menu.index',compact('rolemenus'))
        ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $menus=Menu::orderBy('name')->pluck('name', 'id');
        $roles=Role::orderBy('name')->pluck('name', 'id');
        return view('role-menu.create', compact('menus', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rolemenu=RoleMenu::where('menu_id', $request->menu_id)->where('role_id', $request->role_id)->get();
        if ($rolemenu->count()>0) {
            # code...
            return redirect()->route('role-menu.create')
                        ->with('message','Esta relacion rol-menu ya existe.');
        }
        RoleMenu::create($request->all());
        return redirect()->route('role-menu.index')
                        ->with('message','Role menu agregado satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RoleMenu  $roleMenu
     * @return \Illuminate\Http\Response
     */
    public function show(RoleMenu $roleMenu)
    {
        //
        return view('building');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RoleMenu  $roleMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(RoleMenu $roleMenu)
    {
        //
        $menus=Menu::orderBy('name')->pluck('name', 'id');
        $roles=Role::orderBy('name')->pluck('name', 'id');
        return view('role-menu.create', compact('menus', 'roles', 'roleMenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RoleMenu  $roleMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RoleMenu $roleMenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RoleMenu  $roleMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, RoleMenu $roleMenu)
    {
        //

    //     RoleMenu::where('menu_id', $request->menu_id)->where('role_id', $request->role_id)->delete();
    //     return redirect()->route('item.index')
    //                     ->with('message','Rubro eliminado satisfactoriamente.');
    }
    public function delete(Request $request)
    {
        //

        RoleMenu::where('menu_id', $request->menu_id)->where('role_id', $request->role_id)->delete();
        return redirect()->route('role-menu.index')
                        ->with('message','Rubro eliminado satisfactoriamente.');
    }
}

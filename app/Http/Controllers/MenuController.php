<?php

namespace App\Http\Controllers;

use App\Http\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $menus = Menu::orderBy('position')->paginate(20);
        return view('menu.index',compact('menus'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('menu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 
        if( !\Route::has($request->route) )
         {
            return redirect()->route('menu.create')
                        ->with('message','La ruta ingresada no existe.');
         }
        Menu::create($request->all());
        return redirect()->route('menu.index')
                        ->with('message','Menu agregado satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
        return view('menu.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
        return view('menu.edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        //
        request()->validate([
        ]);
        if( !\Route::has($request->route) )
         {
            return redirect()->route('menu.edit', $menu->id)
                        ->with('message','La ruta ingresada no existe.');
         }

        Menu::find($menu->id)->update($request->all());
        return redirect()->route('menu.index')->with('message','Menu actualizado satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        //
        $menu->delete();
        return redirect()->route('menu.index')
                        ->with('message','Menu eliminado satisfactoriamente.');
    }
}

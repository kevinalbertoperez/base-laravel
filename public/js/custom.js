$( document ).ready(function(){
	// Codigo para las recargas del titulo
	var centovacast = (window.centovacast||(window.centovacast={}));
	(centovacast.streaminfo||(centovacast.streaminfo={})).config = {
	poll_limit: 3000, // specify how many times to poll the server
	poll_frequency: 30000 // specify the poll frequency in milliseconds
	};
	// $('.home').find('a[href^="https://"]').not('a[href*=gusdecool]').attr('target','_blank');
	// $('.home').find('a[href^="http://"]').not('a[href*=gusdecool]').attr('target','_blank');
	$(".button-collapse").sideNav({closeOnClick: true,});
	$('ul.tabs').tabs({swipeable:false});
	$('.collapsible').collapsible();
	$('select').material_select();
	$('.tooltipped').tooltip({delay: 50, html:true});
	$('.parallax').parallax();
	$(".dropdown-button").dropdown();
	$('.carousel.carousel-program').carousel({duration:200,shift:100, indicators:true});
    function slider() {
		$('.carousel.carousel-program').carousel('next');
    	setTimeout(slider , 10000);
    }
	$('.tooltipped').tooltip({delay: 50});
	if ($('.message').text()!="") {
	 Materialize.toast($('.message').text(), 3000, 'rounded')
	}
	if ($('.message-error').html()!="") {
	 Materialize.toast($('.message-error').html(), 5000)
	}
    slider();
	if ($('.message').text()!="") {
	 Materialize.toast($('.message').text(), 3000, 'rounded')
	}
	function print() {
	    window.print();
	}
	function datepicker() {
		$('.datepicker').pickadate({
			selectMonths: true, // Creates a dropdown to control month
			selectYears: 15, // Creates a dropdown of 15 years to control year,
			today: 'Hoy',
			clear: 'Limpiar',
			close: 'Ok',
			format: 'd mmmm, yyyy',
			formatSubmit: 'yyyy-mm-dd',
			hiddenName: true,
			closeOnSelect: false, // Close upon selecting a date,
			monthsFull: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
			weekdaysFull: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
			weekdaysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
			weekdaysLetter: ["D", "L", "M", "X", "J", "V", "S"],
		});
	}
	datepicker();
	$('.timepicker').pickatime({
		default: 'now', // Set default time: 'now', '1:30AM', '16:30'
		fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
		twelvehour: false, // Use AM/PM or 24-hour format
		donetext: 'OK', // text for done-button
		cleartext: 'Limpiar', // text for clear-button
		canceltext: 'Cancelar', // Text for cancel-button
		autoclose: true, // automatic close timepicker
		ampmclickable: true, // make AM PM clickable
		aftershow: function(){} //Function for after opening timepicker
	});
	$('.imagen-preview').click(function (e) {
		$($(this).attr('input')).trigger('click');
	});

    $(".input-file-image").change(function(){
        readURL(this, $(this).attr('view-id'));
    });
	function readURL(input, image) {
		if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function (e) {
		        $(image).css('background-image', 'url('+e.target.result+')');
		    }
		    reader.readAsDataURL(input.files[0]);
		}
	}
	var $window = $(window);
    $window.on('scroll', function() {
        if ($window.scrollTop() > 120) {
        	$('.home .navbar-fixed nav').removeClass('transparent');
        	$('.home .navbar-fixed nav ul li a').removeClass('white-text').addClass('black-text');
        } else {
        	$('.home .navbar-fixed nav').addClass('transparent');
        	$('.home .navbar-fixed nav ul li a').addClass('white-text').removeClass('black-text');
        }
    });
});
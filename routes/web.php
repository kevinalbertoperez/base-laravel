<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'PageController@home')->name('home');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/cookies-policy', 'PageController@cookies_policy')->name('cookies.policy');

Route::middleware(['auth'])->group(function () {
	Route::prefix('dashboard')->group(function () {
		Route::get('/', 'DashboardController@index')->name('dashboard');
		Route::post('/rolemenu/delete', 'RoleMenuController@delete')->name('role-menu.delete');
		Route::resources([
			'configuration'=>'ConfigurationController',
			'user'=>'UserController',
			'menu'=>'MenuController',
			'role'=>'RoleController',
			'role-menu'=>'RoleMenuController',
		]);
	});
});

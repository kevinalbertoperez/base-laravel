@extends('layouts.index')
@section('content')
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col s12">
				<h3 class="extra-bold">Política de Cookies</h3>
				{!! $configuration->cookies !!}
		</div>
	</div>
</div>
@endsection 
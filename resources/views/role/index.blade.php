@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="#!" class="breadcrumb">role</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Roles Registrados</h4>
        <table class="striped highlight responsive-table">
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Slug</th>
                <th>Descripción</th>
                <th>Actiones</th>
            </tr>
            @foreach ($roles as $role)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $role->name}}</td>
                    <td>{{ $role->slug}}</td>
                    <td>{{ $role->descriprion}}</td>
                    <td>
                        <a class="btn red waves-effect waves-light " href="{{ route('role.show',$role->id) }}"><i class="material-icons">remove_red_eye</i></a>
                        <a class="btn red waves-effect waves-light " href="{{ route('role.edit',$role->id) }}"><i class="material-icons">edit</i></a>
                        <!-- {!! Form::open(['method' => 'DELETE','route' => ['role.destroy', $role->id],'style'=>'display:inline']) !!}
                            <button type="submit" class="btn red waves-effect waves-light "><i class="material-icons">delete</i></button>
                        {!! Form::close() !!} -->
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $roles->links() !!}
    </div>
</div>
    <div class="fixed-action-btn"  style="right: 100px;">
        <a href="{{route('role.create')}}" class="btn-floating btn-large pink tooltipped" data-position="left" data-delay="50" data-tooltip="Registrar role"><i class="material-icons">directions_walk</i></a>
    </div>
@endsection
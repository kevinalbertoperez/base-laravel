@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('role.index')}}" class="breadcrumb">Role</a>
                <a href="#!" class="breadcrumb">Editar</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Editar role</h4>

    {!! Form::model($role, ['method' => 'PATCH','route' => ['role.update', $role->id], 'class'=>'col s12', 'enctype'=>"multipart/form-data",]) !!}

        @include('role.form')

    {!! Form::close() !!}
    </div>
</div>
@endsection
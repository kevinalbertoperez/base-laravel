<div class="row">
  <div class="input-field col s12 m6">
    {!! Form::text('name', null, array('id' => 'name')) !!}
    <label for="name">Nombre</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::text('slug', null, array('id' => 'slug')) !!}
    <label for="slug">Slug</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::text('description', null, array('id' => 'description')) !!}
    <label for="description">Descripcion</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <button class="btn red waves-effect waves-light left" type="submit" name="action">Guardar
      <i class="mdi-save right"></i>
    </button>
  </div>
</div>


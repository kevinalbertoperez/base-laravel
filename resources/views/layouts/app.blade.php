<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{asset('img/favicon.png?v=1')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles --> 
    @if(config('app.env', 'production')=='local') 
      <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
      <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    @else
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    @endif
    <link href="{{ asset('css/custom.css?v='.date('is')) }}" rel="stylesheet">

    
</head>
<body>
    <header>
      <nav class="blue darken-3">
          <div class="container">
              <div class="nav-wrapper">
                <a class="brand-logo" href="{{ url('/') }}">
                  {{ config('app.name', 'Laravel') }}
                </a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  @guest
                      <li  class="{{Route::currentRouteName()=='login'?'active':''}}"><a href="{{ route('login') }}">Iniciar sesión</a></li>
                      <li  class="{{Route::currentRouteName()=='register'?'active':''}}"><a href="{{ route('register') }}">Registrar</a></li>
                  @else
                  @endguest
                </ul>
                <ul class="side-nav" id="mobile-demo">
                  <!-- Authentication Links -->
                  @guest
                      <li  class="{{Route::currentRouteName()=='login'?'active':''}}"><a href="{{ route('login') }}">Iniciar sesión</a></li>
                      <li  class="{{Route::currentRouteName()=='register'?'active':''}}"><a href="{{ route('register') }}">Registrar</a></li>
                  @else   
                      @include('layouts.menu')                  
                  @endguest
                </ul>
              </div>
          </div>
      </nav>
    </header>
    <main id="app">
        @yield('content')
    </main>

    <footer class="page-footer" style="padding-top: 0px;">
      <!-- <div class="container">
        <div class="row">
          <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
          </div>
          <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
              <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
              <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
            </ul>
          </div>
        </div>
      </div> -->
      <div class="footer-copyright blue darken-3">
        <div class="container">
        Copyright 2018 <a href="http://elduke.com.ve/" target="_blank" class="white-text bold">El Duke</a> | Todos los Derechos Reservados | <a href="{{route('cookies.policy')}}" class="white-text bold">Política de Cookies</a>
        </div>
      </div>
    </footer>
    <!-- Scripts -->
    @if(config('app.env', 'production')=='local') 
      <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('js/materialize.min.js') }}"></script>
    @else
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    @endif
    <script src="{{ asset('js/custom.js?v='.date('is')) }}"></script>
</body>
</html>

	<li class="collection-item  hide-on-small-only show-on-medium-and-up" style="height: 90px;"><a href="{{ route('home') }}">{{-- <img src="{{asset('img/logo.png')}}" class="responsive-img logo" alt="Logo"> --}} <h4> {{ config('app.name', 'Laravel') }}</h4></a></li>
	@foreach(Auth::user()->role->menu()->orderBy('position','asc')->get() as $menu)
		<li class="collection-item {{Route::currentRouteName()==$menu->route?'active blue darken-3 white-text':''}}">
			<a href="{{route($menu->route)}}" class="{{Route::currentRouteName()==$menu->route?'active blue darken-3 white-text':''}}"><i class="material-icons circle {{Route::currentRouteName()==$menu->route?'active blue darken-3 white-text':''}}">{{$menu->ico}}</i>
				<span class="title">
                    @if($menu->route=='order.index')
                        @if(Auth::user()->warehouse->orders()->where('status', 1)->count())
                            <span class="badge new blue material-icons" data-badge-caption="local_shipping"></span>
                        @endif
                        @if(Auth::user()->warehouse->order()->where('status', 2)->count())
                            <span class="badge new red material-icons" data-badge-caption="check"></span>
                        @endif
                    @endif
                    {{$menu->name}}
                </span>
			</a>
		</li>
	@endforeach
    <li class="collection-item">
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
            <i class="material-icons">do_not_disturb_on</i>  Cerrar sesión
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>  

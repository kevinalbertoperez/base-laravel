 <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/favicon.png?v=1')}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->  
    @if(config('app.env', 'production')=='local')
      <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
      <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    @else
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    @endif
    <link href="{{asset('css/materialnote.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/custom.css?v='.date('is')) }}" rel="stylesheet">

    
</head>
<body class="dashboard">
    <header>
      <div class="message hide">{{session('message')}}</div>
      <div class="message-error hide">@if ($errors->count())<div class="yellow-text lighten-4"><ul>@foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach</ul></div>@endif</div>
      <nav class="blue darken-3  show-on-small hide-on-med-and-up">
          <div class="container">
              <div class="nav-wrapper">
                <a class="brand-logo show-on-small hide-on-med-and-up" href="{{ url('/') }}">
                  {{ config('app.name', 'Laravel') }}
                </a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                
             {{--<ul id="nav-mobile" class="right">
                  <li><a href="collapsible.html"><i class="material-icons">notifications</i></a></li>
                </ul> --}}
                <ul class="side-nav" id="mobile-demo">
                  @guest
                      <li  class="{{Route::currentRouteName()=='login'?'active':''}}"><a href="{{ route('login') }}">Iniciar sesión</a></li>
                      <li  class="{{Route::currentRouteName()=='register'?'active':''}}"><a href="{{ route('register') }}">Registrar</a></li>
                  @else   
                      @include('layouts.menu')                  
                  @endguest
                </ul>
              </div>
          </div>
      </nav>
    </header>
    <main id="app">
      <div class="row">
        <div class="col m2 l2 hide-on-small-only">
          <div class="side-nav fixed">
            <ul class="collection nav-side" style="">
              @include('layouts.menu')
            </ul>
          </div>
        </div>
        <div class="col s12 m12 l12" >
          <div class="main">
            <br>
            @yield('dashboard-content')
          </div>
        </div>
      </div>

      <div class="fixed-action-btn">
        <a class="btn-floating btn-large light-blue darken-3 pulse">
          <i class="large material-icons">add</i>
        </a>
        <ul>


         <li><a href="#!" onclick="print()" class="btn-floating green tooltipped" data-position="left" data-delay="50" data-tooltip="Imprimir"><i class="material-icons">print</i></a></li>
          @if(Auth::user()->role->slug=='admin')
           <li><a href="{{route('configuration.create')}}" class="btn-floating deep-purple darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Configuración"><i class="material-icons">settings</i></a></li>
           @endif
        </ul>
      </div>
  </main>
    @if(config('app.env', 'production')=='local') 
      <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('js/materialize.min.js') }}"></script>
    @else
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    @endif
    <script type="text/javascript" src="{{asset('js/materialnote.js')}}"></script>  
    <script src="{{ asset('js/custom.js?v='.date('is')) }}"></script>
    @section('scripts')
    @show
</body>
</html>
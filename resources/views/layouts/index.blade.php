<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="{{asset('img/favicon.png?v=1')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->  
    @if(config('app.env', 'production')=='local') 
      <link href="{{ asset('css/icon.css') }}" rel="stylesheet">
      <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    @else
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    @endif
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
    <link href="{{ asset('css/custom.css?v='.date('is')) }}" rel="stylesheet">
     
    <!-- Include js plugin -->
</head>
<body  class="{{Route::currentRouteName()}}">
  <div class="loader hide">
    
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>
        
  </div>
    <header class="navbar-fixed" style="height: 80;">
      <div class="message hide">{{session('message')}}</div>
      <nav class="blue darken-3">
          <div class="container">
              <div class="nav-wrapper">
                <a class="brand-logo" href="{{ url('/') }}">
                  {{-- <img src="{{asset('img/logo.png')}}" class="responsive-img logo" alt="Logo"> --}}
                  {{ config('app.name', 'Laravel') }}
                </a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  @guest
                      <li  class="{{Route::currentRouteName()=='login'?'active':''}}"><a href="{{ route('login') }}">Iniciar sesión</a></li>
                      <li  class="{{Route::currentRouteName()=='register'?'active':''}}"><a href="{{ route('register') }}">Registrar</a></li>
                  @else
                  @endguest
                </ul>
                <ul class="side-nav" id="mobile-demo">
                  @guest
                      <li  class="{{Route::currentRouteName()=='login'?'active':''}}"><a href="{{ route('login') }}">Iniciar sesión</a></li>
                      <li  class="{{Route::currentRouteName()=='register'?'active':''}}"><a href="{{ route('register') }}">Registrar</a></li> 
                  @else
                  @endguest
                  
                </div>
          </div>
      </nav>
    </header>
    <main id="app">
        @yield('content')
    </main>
<footer class="page-footer blue accent-4" style="padding-top: 0px;">
      <div class="container">
        <div class="row">
          <div class="col s12 m4"></div>
          <div class="col s12 m4"></div>
          <div class="col s12 m4"></div>
        </div>
      </div>
      <div class="footer-copyright blue accent-4">
        <div class="container">
        Copyright 2018 <a href="http://elduke.com.ve/" target="_blank" class="white-text bold">El Duke</a> | Todos los Derechos Reservados | <a href="{{route('cookies.policy')}}" class="white-text bold">Política de Cookies</a>
        </div>
      </div>
    </footer>
    <div class="cookies-policy white">
      <div class="">
        <div class="row center-align" style="margin-bottom: 0px;">
          En cumplimiento con la Ley 34/2002 de servicios de la sociedad de la información te recordamos que al navegar por este sitio estás aceptando el uso de cookies. <a href="#" class="btn black done">Aceptar</a> <a href="{{route('cookies.policy')}}" class="black-text bold">Leer Más</a>
          
        </div>
      </div>
    </div>
    <!-- Scripts -->
    @if(config('app.env', 'production')=='local') 
      <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
      <script src="{{ asset('js/materialize.min.js') }}"></script>
    @else
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="{{ asset('js/custom.js?v='.date('is')) }}"></script>
    <script>
      $(function () {
        // body...

        if ($.cookie("cookies-policy")==1) {
          $('.cookies-policy').remove();
        }
        $('.cookies-policy a.done').click(function (e) {
          $.cookie("cookies-policy", 1, {expires : 15});
          $('.cookies-policy').remove();
          e.preventDefault();
        });
      });
    </script>
  @section('scripts')
  @show
</body>
</html>

@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-3">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('configuration.index')}}" class="breadcrumb">Configuración</a>
                <a href="#!" class="breadcrumb">Editar</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Editar categoria</h4>

      {!! Form::model($configuration, ['method' => 'PATCH','route' => ['configuration.update', $configuration->id], 'class'=>'col s12', 'enctype'=>"multipart/form-data",]) !!}
        @include('configuration.form')

    {!! Form::close() !!}
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $.each($('.wysiwyg'), function(index, node) {
            $(node).materialnote({
                height: 300,
                posIndex: index
            });
        });
        })
    </script>
@stop
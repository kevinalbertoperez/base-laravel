<div class="row">
  <div class="input-field col s12">
    {!! Form::text('key', null, array('id' => 'key', 'readonly', 'disable')) !!}
    <label for="key">Nombre</label>
  </div>
  <div class="input-field col s12">
    <h4 for="key_value">Valor</h4>
    {!! Form::textarea('key_value', null, array('class' => 'wysiwyg hide', 'data-wysiwyg'=>'materialnote', 'id' => 'key_value')) !!}
  </div>
  <div class="input-field col s12">
    <h4 for="key_description">Descripcion</h4>
    {!! Form::textarea('key_description', null, array('class' => 'wysiwyg hide', 'data-wysiwyg'=>'materialnote', 'id' => 'key_description')) !!}
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <button class="btn red waves-effect waves-light" type="submit" name="action">Guardar
      <i class="mdi-save right"></i>
    </button>
  </div>
</div>


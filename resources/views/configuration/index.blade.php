@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-3">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="#!" class="breadcrumb">Configuración</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Configuraciones registradas</h4>
        <table class="striped highlight responsive-table">
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
            @foreach ($configurations as $configuration)
                <tr>
                    <td>{{ $configuration->id }}</td>
                    <td>
                            {{ $configuration->key}}
                    </td>
                    <td>
                        <a class="btn waves-effect waves-light " href="{{ route('configuration.show',$configuration->id) }}"><i class="material-icons">remove_red_eye</i></a>
                        <a class="btn waves-effect waves-light " href="{{ route('configuration.edit',$configuration->id) }}"><i class="material-icons">edit</i></a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
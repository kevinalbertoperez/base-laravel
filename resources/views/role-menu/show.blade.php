@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('role.index')}}" class="breadcrumb">Roles</a>
                <a href="#!" class="breadcrumb">{{$role->name}}</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Rol {{$role->name}}</h4>

        <h5>Nombre</h5>
        <p>{{$role->name}}</p>
        <h5>Descripción</h5>
        <p>{{$role->description}}</p>
    </div>
    <div class="col  s12 m6 ">
	    <a class="btn red waves-effect waves-light " href="{{ route('role.edit',$role->id) }}"><i class="material-icons">edit</i></a>
	    {!! Form::open(['method' => 'DELETE','route' => ['role.destroy', $role->id],'style'=>'display:inline']) !!}
	    <button type="submit" class="btn red waves-effect waves-light "><i class="material-icons">delete</i></button>
	    {!! Form::close() !!}
    </div>
</div>
@endsection
<div class="row">
  <div class="input-field col s12 m6">
    {!! Form::select('menu_id', $menus,null, array('id' => 'menu_id')) !!}
    <label for="menu_id">Menu</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::select('role_id', $roles,null, array('id' => 'role_id')) !!}
    <label for="role_id">Role</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <button class="btn red waves-effect waves-light left" type="submit" name="action">Guardar
      <i class="mdi-save right"></i>
    </button>
  </div>
</div>


@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="#!" class="breadcrumb">Roles-Menu</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Roles-Menu Registrados</h4>
        <table class="striped highlight responsive-table">
            <tr>
                <th>No</th>
                <th>Menu</th>
                <th>Role</th>
                <th>Actiones</th>
            </tr>
            @foreach ($rolemenus as $rolemenu)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $rolemenu->menu->name}}</td>
                    <td>{{ $rolemenu->role->name}}</td>
                    <td>
                        <a class="btn red waves-effect waves-light " href="{{ route('role-menu.show',$rolemenu->id) }}"><i class="material-icons">remove_red_eye</i></a>
                        <a class="btn red waves-effect waves-light " href="{{ route('role-menu.edit',$rolemenu->id) }}"><i class="material-icons">edit</i></a>
                        {!! Form::open(['method' => 'POST','route' => ['role-menu.delete'],'style'=>'display:inline']) !!}
                            {!! Form::hidden('role_id', $rolemenu->role_id) !!}
                            {!! Form::hidden('menu_id', $rolemenu->menu_id) !!}
                            <button type="submit" class="btn red waves-effect waves-light "><i class="material-icons">delete</i></button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $rolemenus->links() !!}
    </div>
</div>
    <div class="fixed-action-btn"  style="right: 100px;">
        <a href="{{route('role-menu.create')}}" class="btn-floating btn-large  light-green tooltipped" data-position="left" data-delay="50" data-tooltip="Registrar Role-Menu"><i class="material-icons">explore</i></a>
    </div>
@endsection
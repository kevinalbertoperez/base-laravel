@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1 ">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('role-menu.index')}}" class="breadcrumb">Rol-Menu</a>
                <a href="#!" class="breadcrumb">Nuevo</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Nuevo rol-menu</h4>

    {!! Form::open(array('route' => 'role-menu.store','method'=>'POST', 'enctype'=>"multipart/form-data")) !!}

        @include('role-menu.form')

    {!! Form::close() !!}
    </div>
</div>
@endsection
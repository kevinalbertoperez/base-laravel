@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('menu.index')}}" class="breadcrumb">Menu</a>
                <a href="#!" class="breadcrumb">Editar</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Editar menu</h4>

    {!! Form::model($menu, ['method' => 'PATCH','route' => ['menu.update', $menu->id], 'class'=>'col s12', 'enctype'=>"multipart/form-data",]) !!}

        @include('menu.form')

    {!! Form::close() !!}
    </div>
</div>
@endsection
<div class="row">
  <div class="input-field col s12 m6">
    {!! Form::text('name', null, array('id' => 'name')) !!}
    <label for="name">Nombre</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::text('route', null, array('id' => 'route')) !!}
    <label for="route">Ruta</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::text('ico', null, array('id' => 'ico')) !!}
    <label for="ico">Icono</label>
  </div>
  <div class="input-field col s12 m6">
    {!! Form::text('position', null, array('id' => 'position')) !!}
    <label for="position">Posición</label>
  </div>
</div>
<div class="row">
  <div class="input-field col s12">
    <button class="btn red waves-effect waves-light left" type="submit" name="action">Guardar
      <i class="mdi-save right"></i>
    </button>
  </div>
</div>


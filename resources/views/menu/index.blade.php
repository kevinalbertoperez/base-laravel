@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="#!" class="breadcrumb">Menu</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Menu Registrados</h4>
        <table class="striped highlight responsive-table">
            <tr>
                <th>No</th>
                <th>Nombre</th>
                <th>Ruta</th>
                <th>Icono</th>
                <th>Posición</th>
                <th>Actiones</th>
            </tr>
            @foreach ($menus as $menu)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $menu->name}}</td>
                    <td>{{ $menu->route}}</td>
                    <td><i class="material-icons">{{$menu->ico}}</i></td>
                    <td>{{ $menu->position}}</td>
                    <td>
                        <a class="btn red waves-effect waves-light " href="{{ route('menu.show',$menu->id) }}"><i class="material-icons">remove_red_eye</i></a>
                        <a class="btn red waves-effect waves-light " href="{{ route('menu.edit',$menu->id) }}"><i class="material-icons">edit</i></a>
                        {!! Form::open(['method' => 'DELETE','route' => ['menu.destroy', $menu->id],'style'=>'display:inline']) !!}
                            <button type="submit" class="btn red waves-effect waves-light "><i class="material-icons">delete</i></button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $menus->links() !!}
    </div>
</div>
    <div class="fixed-action-btn"  style="right: 100px;">
        <a href="{{route('menu.create')}}" class="btn-floating btn-large purple tooltipped" data-position="left" data-delay="50" data-tooltip="Registrar menu"><i class="material-icons">dns</i></a>
    </div>
@endsection
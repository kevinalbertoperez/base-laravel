@extends('layouts.dashboard')

@section('dashboard-content')   
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-1">
            <div class="nav-wrapper">
              <div class="col s12 m12 l12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
                <a href="{{route('menu.index')}}" class="breadcrumb">Menus</a>
                <a href="#!" class="breadcrumb">{{$menu->name}}</a>
              </div>
            </div>
        </nav>
    </div>
    <div class="col  s12 m12 l12">
        <h4 class="center-align">Proveedor {{$menu->name}}</h4>

        <h5>Nombre</h5>
        <p>{{$menu->name}}</p>
        <h5>Ruta</h5>
        <p>{{$menu->route}}</p>
        <h5>Icono</h5>
        <p>{{$menu->ico}}</p>
        <h5>Posicion</h5>
        <p>{{$menu->position}}</p>
    </div>
    <div class="col  s12 m6 ">
	    <a class="btn red waves-effect waves-light " href="{{ route('menu.edit',$menu->id) }}"><i class="material-icons">edit</i></a>
	    {!! Form::open(['method' => 'DELETE','route' => ['menu.destroy', $menu->id],'style'=>'display:inline']) !!}
	    <button type="submit" class="btn red waves-effect waves-light "><i class="material-icons">delete</i></button>
	    {!! Form::close() !!}
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col  col s12 m8 offset-m2">
            <div class="card blue darken-1">
                <div class="card-content white-text">
                    <span class="card-title">Register</span>
                    <div class="">
                    <form class="" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="input-field white-field col s12 {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="" name="name" value="{{ old('name') }}" required autofocus>

                            <label for="name" class="">Name</label>
                        </div>
                        <div class="col s12">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="input-field white-field col s12 {{ $errors->has('email') ? ' has-error' : '' }} ">
                            <input id="email" type="email" class="" name="email" value="{{ old('email') }}" required>
                            <label for="email" class="">E-Mail Address</label>

                        </div>
                        <div class="col s12">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class=" input-field white-field col s12  {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" type="password" class="" name="password" required>
                            <label for="password" class="">Password</label>

                        </div>
                        <div class="col s12">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="input-field white-field col s12">
                                <input id="password-confirm" type="password" class="" name="password_confirmation" required>
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                        </div>

                            <div class="col-md-6">
                            </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn red">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

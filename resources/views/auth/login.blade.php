@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col  col s12 m8 offset-m2">
            <div class="card blue darken-1">
                <div class="card-content white-text">
                    <span class="card-title">Iniciar sesión</span>
                    <div class="">
                        <form class="" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="">

                                <div class="input-field white-field col s12">
                                  <input id="email" name="email" type="text" class="validate" value="{{ old('email') }}" required autofocus>
                                  <label for="email">Email</label>
                                </div>
                            
                                @if ($errors->has('email'))
                                    <span class="red-text accent-2">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="">

                                <div class="input-field white-field col s12">
                                  <input id="password"  name="password" type="password" class="validate">
                                  <label for="password">Contraseña</label>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="red-text accent-2">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="">
                                <div class="switch white-field">
                                    <label>
                                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                      <span class="lever"></span>
                                      Recordarme
                                    </label>
                                </div>
                                <br>
                            </div>

                            
                            <d iv class="card-action">
                                <div class="">
                                    <button type="submit" class="btn red">
                                        Iniciar sesión
                                    </button>

                                    <a class="btn red" href="{{ route('password.request') }}">
                                        Recuperar contraseña
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.dashboard')

@section('dashboard-content')
<div class="row">
    <div class="col s12 m12 l12">
        <nav class="blue darken-3">
            <div class="nav-wrapper">
              <div class="col s12">
                <a href="{{route('dashboard')}}" class="breadcrumb">Panel de control</a>
              </div>
            </div>
        </nav>
        <div class="center-align">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h4>Bienvenido {{Auth::user()->name}}</h4>
                    <h5>Eres el {{Auth::user()->role->name}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('roles')->insert([
            'name' => 'Administrador',
            'description' => 'Administrador',
            'slug' => 'admin'
        ]);
        DB::table('roles')->insert([
            'name' => 'Sudo',
            'description' => 'Administracion de menu y acceso.',
            'slug' => 'sudo'
        ]);
        DB::table('roles')->insert([
            'name' => 'User',
            'description' => 'Usuario.',
            'slug' => 'user'
        ]);
        //Menu
        //Menu 1
        DB::table('menus')->insert([
            'name' => 'Dashboard',
            'route' => 'dashboard',
            'ico' => 'dashboard',
            'position' => 0,
        ]);
        //Menu 2
        DB::table('menus')->insert([
            'name' => 'Configuracion',
            'route' => 'configuration.index',
            'ico' => 'settings',
            'position' => 10,
        ]);
        //Menu 3
        DB::table('menus')->insert([
            'name' => 'Usuarios',
            'route' => 'user.index',
            'ico' => 'people',
            'position' => 1,
        ]);
        //Menu 4
        DB::table('menus')->insert([
            'name' => 'Menus',
            'route' => 'menu.index',
            'ico' => 'dns',
            'position' => 1,
        ]);
        //Menu 5
        DB::table('menus')->insert([
            'name' => 'Roles',
            'route' => 'role.index',
            'ico' => 'directions_walk',
            'position' => 2,
        ]);
        //Menu 6
        DB::table('menus')->insert([
            'name' => 'Roles-Menus',
            'route' => 'role-menu.index',
            'ico' => 'explore',
            'position' => 3,
        ]);
        //Fin menu
        
        //Inicio Roles-Menu
            //Role 1 Administrador
            //Role 2 Cliente
            //Role 3 Sudo
            //Role 4 

            //Menu 1 Dashboard
            //Menu 2 Configuracion
            //Menu 3 Usuarios
            //Menu 4 Menus
            //Menu 5 Roles
            //Menu 6 Roles-Menus

            DB::table('role_menus')->insert([
                'role_id' => 1,
                'menu_id' => 1
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 1,
                'menu_id' => 2
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 1,
                'menu_id' => 3
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 3,
                'menu_id' => 1
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 2,
                'menu_id' => 4
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 2,
                'menu_id' => 5
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 2,
                'menu_id' => 6 
            ]);
            DB::table('role_menus')->insert([
                'role_id' => 2,
                'menu_id' => 1
            ]);
           
            //User 1
            DB::table('users')->insert([
                'name' => 'Administrador',
                'email' => 'administrador@base.com',
                'role_id' => 1,
                'password' => bcrypt('890123'),
            ]);
            //User 2
            DB::table('users')->insert([
                'name' => 'Sudo',
                'email' => 'sudo@base.com',
                'role_id' => 2,
                'password' => bcrypt('890123'),
            ]);
            DB::table('configurations')->insert([
                'key' =>'name', 
                'key_value' =>'Base',
                'key_description' =>'Nombre del sistema',
            ]);
            DB::table('configurations')->insert([
                'key' =>'email', 
                'key_value' =>'usuario@usuario.com',
                'key_description' =>'Correo del sistema',
            ]);
    }
}
